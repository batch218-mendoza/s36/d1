const express =  require("express");
const mongoose = require("mongoose");

const taskRoute =require("./routes/taskRoute.js");

// models folder >> task.js
// controllers folder >> taskController.js
// route folder >> taskRoute.js

/*

	GitBash :
		npm init -y
		npm i express
		npm i mongoose
		touch .gitignore > content: node_modules

*/


// Server Setup
const app = express();
const port = 3001;

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
// DB connection
mongoose.connect("mongodb+srv://admin:admin@batch218-to-do.c4bzfiz.mongodb.net/toDo?retryWrites=true&w=majority",

{

	useNewurlParser: true,
	useUnifiedTopology: true

});

app.use("/tasks", taskRoute);
app.use("/addNewTask", taskRoute);


app.listen(port, () => console.log(`Now listening to port ${port}`));

//gitbash:
// start node application
//nodemon index.js
