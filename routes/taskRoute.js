//this document contains all the endpoints for our application also http methods

const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js");

router.get("/viewTasks",(req,res) =>{
	//invokes the "getAllTasks" function from the "taskcontroller.js" file sends the result back to

									//returns result from our controller
	taskController.getAllTasks().then(result =>
		res.send(result));
})


router.post("/addNewTask", (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result))
})

							//: id will serve as our wildcard
router.delete("/deleteTask/:id", (req,res) => {
taskController.deleteTask(req.params.id).then(result => res.send(
	result))
});


router.put("/updateTask/:id", (req,res) => {
taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
});


router.get("/getOneTask/:id", (req,res) => {
taskController.getOneTask(req.params.id).then(result => res.send(result));
});


router.put("/updateStatus/:id", (req,res) => {
taskController.statusUpdate(req.params.id, req.body).then(result => res.send(result));
});

module.exports = router;


